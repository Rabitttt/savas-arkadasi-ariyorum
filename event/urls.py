from django.urls import path
from event.views import EventCreateView, EventListView, EventDetailView, TeamDetailView, TeamCreateView, TeamActionView, \
    EventActionView, EventUpdateView, TeamUpdateView

urlpatterns = [
    path('create/', EventCreateView.as_view(), name='event_create'),
    path('list/', EventListView.as_view(), name='event_list'),
    path('detail/<int:event_id>/', EventDetailView.as_view(), name='event_detail'),
    path('detail/<int:event_id>/remove/', EventActionView.as_view(action="remove", validate=True), name='event_remove'),
    path('detail/<int:event_id>/edit/', EventActionView.as_view(action="edit"), name='event_edit'),
    path('detail/<int:event_id>/edit_form/', EventUpdateView.as_view(), name='event_edit_form'),
    path('detail/<int:event_id>/team/create/', TeamCreateView.as_view(), name='team_create'),
    path('detail/<int:event_id>/team/<int:team_id>/', TeamDetailView.as_view(), name='team_detail'),
    path('detail/<int:event_id>/team/<int:team_id>/join/', TeamActionView.as_view(action="join"), name='team_join'),
    path('detail/<int:event_id>/team/<int:team_id>/edit/', TeamActionView.as_view(action="edit"), name='team_edit'),
    path('detail/<int:event_id>/team/<int:team_id>/edit_form/', TeamUpdateView.as_view(), name='team_edit_form'),
    path('detail/<int:event_id>/team/<int:team_id>/leave/', TeamActionView.as_view(action="leave"), name='team_leave'),
    path('detail/<int:event_id>/team/<int:team_id>/disband/',
         TeamActionView.as_view(action="disband", validate=True), name='team_disband'),
]
