from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_noop

from event.models import Event, Team, Participation
from django.views.generic import CreateView, ListView, DetailView, RedirectView, UpdateView
from event.forms import EventCreateForm, TeamCreateForm, EventUpdateForm, TeamUpdateForm


class EventListView(ListView):
    model = Event


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventCreateForm
    success_url = reverse_lazy("event_list")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        return kwargs


class EventDetailView(DetailView):
    model = Event
    pk_url_kwarg = "event_id"


class EventUpdateView(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventUpdateForm
    success_url = reverse_lazy("event_list")
    pk_url_kwarg = "event_id"

    def get_success_url(self):
        return reverse("event_detail", kwargs={"event_id": self.kwargs["event_id"]})


class EventActionView(LoginRequiredMixin, RedirectView):
    action = None
    validate = False
    url = None

    def get_redirect_url(self, *args, **kwargs):
        return self.url or reverse("event_detail", kwargs={'event_id': self.kwargs["event_id"]})

    def apply_action(self, action):
        if action == gettext_noop("remove"):
            self.event.delete()
            self.url = reverse("event_list")
        elif action == gettext_noop("edit"):
            self.url = reverse("event_edit_form", kwargs={'event_id': self.kwargs["event_id"]})
        else:
            pass

    def get(self, request, *args, **kwargs):
        self.event = Event.objects.get(id=self.kwargs["event_id"])
        if request.GET.get("valid") == 'false':
            return super().get(request, *args, **kwargs)
        elif self.validate and not request.GET.get("valid") == 'true':
            return render(request, "base/validate.html", {"action": self.action, "object": self.event})
        else:
            self.apply_action(self.action)
            return super().get(request, *args, **kwargs)


class TeamCreateView(LoginRequiredMixin, CreateView):
    model = Team
    form_class = TeamCreateForm
    pk_url_kwarg = "team_id"

    def get_success_url(self):
        return reverse("team_detail", kwargs={"event_id": self.kwargs["event_id"], "team_id": self.object.id})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["request"] = self.request
        kwargs["event"] = Event.objects.get(id=self.kwargs["event_id"])
        return kwargs


class TeamDetailView(ListView):
    model = Participation
    pk_url_kwarg = "team_id"

    def get_queryset(self):
        return super().get_queryset().filter(team_id=self.kwargs["team_id"], team__event_id=self.kwargs["event_id"])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context["team"] = Team.objects.get(id=self.kwargs["team_id"])
        context["event"] = Event.objects.get(id=self.kwargs["event_id"])
        context["users"] = self.object_list.values_list("participant_id", flat=True)
        return context


class TeamUpdateView(LoginRequiredMixin, UpdateView):
    model = Team
    form_class = TeamUpdateForm
    pk_url_kwarg = "team_id"

    def get_success_url(self):
        return reverse("team_detail", kwargs={"event_id": self.kwargs["event_id"], "team_id": self.object.id})


class TeamActionView(LoginRequiredMixin, RedirectView):
    action = None
    validate = False
    url = None

    def get_redirect_url(self, *args, **kwargs):
        return self.url or reverse("team_detail",
                                   kwargs={'event_id': self.kwargs["event_id"], 'team_id': self.kwargs["team_id"]})

    def apply_action(self, action):
        if action == gettext_noop("join"):
            Participation.objects.create(participant=self.request.user, event=self.event, team=self.team)
        elif action == gettext_noop("edit"):
            self.url = reverse("team_edit_form",
                               kwargs={'event_id': self.kwargs["event_id"], 'team_id': self.kwargs["team_id"]})
        elif action == gettext_noop("leave"):
            Participation.objects.filter(participant=self.request.user, event=self.event, team=self.team).delete()
        elif action == gettext_noop("disband"):
            self.team.delete()
            self.url = reverse("event_detail", kwargs={'event_id': self.kwargs["event_id"]})
        else:
            pass

    def get(self, request, *args, **kwargs):
        self.team = Team.objects.get(id=self.kwargs["team_id"])
        self.event = Event.objects.get(id=self.kwargs["event_id"])
        if request.GET.get("valid") == 'false':
            return super().get(request, *args, **kwargs)
        elif self.validate and not request.GET.get("valid") == 'true':
            return render(request, "base/validate.html", {"object": self.team, "action": self.action})
        else:
            self.apply_action(self.action)
            return super().get(request, *args, **kwargs)
