from django import forms
from event.models import Event, Team, Participation
from users.forms import RealDateInput


class EventCreateForm(forms.ModelForm):
    date = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.owner = self.request.user
        obj.save(commit)
        return obj

    class Meta:
        model = Event
        fields = ['name', 'date', 'location', 'description', ]


class EventUpdateForm(forms.ModelForm):
    date = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])
    class Meta:
        model = Event
        fields = ['name', 'date', 'location', 'description']


class TeamCreateForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        self.event = kwargs.pop("event")
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        obj = super().save(commit=False)
        obj.event = self.event
        obj.owner = self.request.user
        obj.save(True)
        Participation.objects.create(participant=obj.owner, event=self.event, team=obj)
        return obj

    class Meta:
        model = Team
        fields = ['name']


class TeamUpdateForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ['name']
