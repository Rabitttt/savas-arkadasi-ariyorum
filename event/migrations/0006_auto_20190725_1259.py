# Generated by Django 2.2.3 on 2019-07-25 12:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0005_auto_20190723_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parctipication',
            name='team',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='event.Team'),
        ),
    ]
