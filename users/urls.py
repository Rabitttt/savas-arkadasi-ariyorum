from django.urls import path

from .views import RegisterView, CharacterUpdateView, ProfileUpdateView, UserDetailView

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('profile/', ProfileUpdateView.as_view(), name='update_profile'),
    path('profile/character/', CharacterUpdateView.as_view(), name='update_character'),
    path('detail/me/', UserDetailView.as_view(is_me=True), name='me'),
    path('detail/<int:pk>/', UserDetailView.as_view(), name='user_detail'),
]
