from django import forms

from .models import UserTable
from django.contrib.auth.forms import UserCreationForm


class RealDateInput(forms.DateInput):
    input_type = "date"


class RegisterForm(UserCreationForm):
    birthday = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta:
        model = UserTable
        fields = UserCreationForm.Meta.fields + (
            "email", "first_name", "last_name", "telephone", "birthday", "location", "image", "biography")


class CharacterUpdateForm(forms.ModelForm):
    class Meta(UserTable.Meta):
        model = UserTable
        fields = [
            'character_name', 'character_class', 'character_level', 'character_biography'
        ]


class ProfileUpdateForm(forms.ModelForm):
    birthday = forms.DateField(widget=RealDateInput, input_formats=['%Y-%m-%d'])

    class Meta(UserTable.Meta):
        model = UserTable
        fields = [
            'first_name', 'last_name', 'email', 'telephone', 'birthday', 'location', 'image', 'biography'
        ]
