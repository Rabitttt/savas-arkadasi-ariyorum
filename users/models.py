from collections import defaultdict

from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.translation import gettext_lazy as _
from event.models import Event


class UserTable(AbstractUser):
    telephone = PhoneNumberField(blank=True, null=True, verbose_name=_("Telephone"))
    birthday = models.DateField(null=True, blank=True, verbose_name=_("Birthday"))
    location = models.CharField(null=True, max_length=20, blank=True, verbose_name=_("Location"))
    image = models.ImageField(null=True, blank=True, upload_to="usertable_image", verbose_name=_("Image"))
    biography = models.TextField(null=True, max_length=500, blank=True, verbose_name=_("Biography"))
    character_name = models.CharField(null=True, max_length=25, blank=True, verbose_name=_("Character Name"))
    character_class = models.CharField(null=True, max_length=20, blank=True, verbose_name=_("Character Class"))
    character_level = models.CharField(null=True, max_length=3, blank=True, verbose_name=_("Character Level"))
    character_biography = models.TextField(null=True, max_length=280, blank=True, verbose_name=_("Character Biography"))
    followed = models.ManyToManyField('self', related_name='followers', verbose_name=_("Followed"))

    def get_absolute_url(self):
        return reverse('user_detail', kwargs={'pk': self.pk})

    def unread_messages(self):
        return self.messages_to_me.filter(seen__isnull=True)

    @property
    def messages_related_to_me(self):
        from communication.models import Messages
        from django.db.models import Q
        return Messages.objects.filter(Q(sender=self) | Q(receiver=self)).order_by("created")

    def get_message_groups(self):
        groups = defaultdict(list)
        for message in self.messages_related_to_me:
            if message.sender == self:
                groups[message.receiver].append(message)
            elif message.receiver == self:
                groups[message.sender].append(message)
            else:
                raise Exception()
        groups.default_factory = None
        return groups
