from django.conf import settings
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include, re_path, reverse_lazy
from django.views.generic import TemplateView
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('users.urls')),
    path('logged_out/', TemplateView.as_view(template_name="registration/logged_out.html"), name="logged_out"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(next_page=reverse_lazy("logged_out")), name="logout"),
    path('event/', include('event.urls')),
    path('rental/', include('rental.urls')),
    path('communication/', include('communication.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('', TemplateView.as_view(template_name="base/homepage.html"), name="homepage"),

]

if settings.DEBUG:
    urlpatterns.append(re_path(r'^media/(?P<path>.*)$',
                               serve,
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}))
