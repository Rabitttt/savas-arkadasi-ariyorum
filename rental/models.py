from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _


class Items(models.Model):
    TYPES = (
        (1, _("Weapon")),
        (2, _("Armor")),
        (3, _("Shield")),
        (4, _("Equipment")),
        (5, _("Special")),
    )

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Owner"))
    name = models.CharField(max_length=30, verbose_name=_("Name"))
    type = models.IntegerField(choices=TYPES, verbose_name=_("Type"))
    image = models.ImageField(null=True, blank=True, verbose_name=_("Image"))
    description = models.TextField(max_length=300, verbose_name=_("Description"))
    is_available_for_rental = models.BooleanField(default=True, verbose_name=_("Is available for rental"))

    def __str__(self):
        return self.name


class ItemRental(models.Model):
    item = models.ForeignKey(Items, on_delete=models.SET_NULL, null=True, verbose_name=_("Item"))
    hirer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, verbose_name=_("Hirer"))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_("Created"))
    fulfilled = models.BooleanField(default=False, verbose_name=_("Fulfilled"))
