from django.contrib import admin
from .models import Items, ItemRental


admin.site.register(Items)
admin.site.register(ItemRental)
