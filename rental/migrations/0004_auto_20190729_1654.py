# Generated by Django 2.2.3 on 2019-07-29 16:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rental', '0003_itemrental_fulfilled'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itemrental',
            name='created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Created'),
        ),
        migrations.AlterField(
            model_name='itemrental',
            name='fulfilled',
            field=models.BooleanField(default=False, verbose_name='Fulfilled'),
        ),
        migrations.AlterField(
            model_name='itemrental',
            name='hirer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Hirer'),
        ),
        migrations.AlterField(
            model_name='itemrental',
            name='item',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='rental.Items', verbose_name='Item'),
        ),
        migrations.AlterField(
            model_name='items',
            name='description',
            field=models.TextField(max_length=300, verbose_name='Description'),
        ),
        migrations.AlterField(
            model_name='items',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='', verbose_name='Image'),
        ),
        migrations.AlterField(
            model_name='items',
            name='is_available_for_rental',
            field=models.BooleanField(default=True, verbose_name='Is available for rental'),
        ),
        migrations.AlterField(
            model_name='items',
            name='name',
            field=models.CharField(max_length=30, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='items',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Owner'),
        ),
        migrations.AlterField(
            model_name='items',
            name='type',
            field=models.IntegerField(choices=[(1, 'Weapon'), (2, 'Armor'), (3, 'Shield'), (4, 'Equipment'), (5, 'Special')], verbose_name='Type'),
        ),
    ]
