from django.urls import path

from rental.views import ItemDetailView, ItemListView, ItemActionView, ItemCreateView, ItemUpdateView

urlpatterns = [
    path("list/", ItemListView.as_view(), name="item_list"),
    path("create/", ItemCreateView.as_view(), name="item_create"),
    path("detail/<int:pk>/", ItemDetailView.as_view(), name="item_detail"),
    path("detail/<int:pk>/edit_form/", ItemUpdateView.as_view(), name="item_edit_form"),
    path("detail/<int:pk>/accept/<int:rental_pk>/", ItemActionView.as_view(action="accept"), name="item_accept"),
    path("detail/<int:pk>/rent/", ItemActionView.as_view(action="rent"), name="item_rent"),
    path("detail/<int:pk>/switch/", ItemActionView.as_view(action="switch"), name="item_switch"),
    path("detail/<int:pk>/edit/", ItemActionView.as_view(action="edit"), name="item_edit"),
    path("detail/<int:pk>/remove/", ItemActionView.as_view(action="remove", validate=True), name="item_remove")
]